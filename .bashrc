# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
export LM_LICENSE_FILE=2100@sambava.ics.ele.tue.nl:1717@o3.ics.ele.tue.nl:5280@sambava.ics.ele.tue.nl

#path to modelsim
export PATH=$PATH:/opt/Mentor/Modelsim/10.1a/modeltech/linux_x86_64

#PATH TO LLVM ON ES-EDA
export SOLVER_LLVM_PATH=/home/solver/local/bin

#simd toolchain
export PATH=$PATH:~/simd-toolchain/build/bin
#export SOLVER_LLVM_PATH=`llvm-config --bindir`
export SOLVER_PATH=~/simd-toolchain/build/bin
export SOLVER_TARGET_ROOT=~/simd-toolchain/solver
export SOLVER_INCLUDE_PATH=~/simd-toolchain/solver/include
export SOLVER_LIB_PATH=~/simd-toolchain/solver/lib

