# Simple Demo:
#
# Control processor adds 5+4 and stores
# it in memory address 0x0
#
# Each PE adds 1 to its pe_id (r1) and
# stores it at address 0x0
#
# The NOPs behind the store word (sw)
# instruction are to ensure that the 
# result is committed to memory before
# simulation is halted
#
# The self loop at the end (j 0) 
# signals the testbench/simulator that
# the end of the program has been 
# reached.

add --, r0, 5
add --, ALU, 4	|| v.add --, r1, 1
sw r0, ALU, 0	|| v.sw r0, ALU, 0
nop		|| v.nop
nop		|| v.nop

j 0
nop
