        .text
        .global        __start
        .type          __start,@function
        .ent           __start
__start: # BB_0:
        simm           128                        # simm  128
        add            r1, r0, 0                  # V_0 = add  V_10000(ZERO), 0
        simm           32                         # simm  32
        add            r2, r0, 0                  # V_0 = add  V_10000(ZERO), 0
        jal            main                     
        nop                                     
        j              0                        
        j              0                        
        nop                                     
        .end           __start

        .global        main
        .type          main,@function
        .ent           main
main: # BB_0:        .exit    .entry
        add            r1, r1, -8                 # V_0 = add  V_0(SP), -8
        add            r11, r0, 0                 # V_3(v0) = add  V_10000(ZERO), 0
        add            --, r0, 5                  # V_9 = add  V_10000(ZERO), 5
        sw             r1, ALU, 0                 # sw  V_0(SP), V_9, 0
        add            --, r0, 4                  # V_8 = add  V_10000(ZERO), 4
        sw             r1, ALU, 1                 # sw  V_0(SP), V_8, 1
        lw             WB, r1, 1                  # V_6 = lw  V_0(SP), 1
        lw             --, r1, 0                  # V_7 = lw  V_0(SP), 0
        add            WB, WB, LSU                # V_5 = add  V_6, V_7
        add            --, r0, 0                  # V_4 = add  V_10000(ZERO), 0
        sw             ALU, WB, 0                 # sw  V_4, V_5, 0
        add            r1, r1, 8                  # V_0 = add  V_0(SP), 8
        jr             r9                         # jr  V_2(RA)
        nop                                     
        .end           main

        .data
        .type          mem, @object
        .global        mem
        .address       0
        .comm          mem, 40

