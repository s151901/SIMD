# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/unittest/SIR/SIRBBTest.cc" "/home/boyan/simd-toolchain/build/unittest/SIR/CMakeFiles/SIRTest.dir/SIRBBTest.cc.o"
  "/home/boyan/simd-toolchain/unittest/SIR/SIRExprTest.cc" "/home/boyan/simd-toolchain/build/unittest/SIR/CMakeFiles/SIRTest.dir/SIRExprTest.cc.o"
  "/home/boyan/simd-toolchain/unittest/SIR/SIRFuncTest.cc" "/home/boyan/simd-toolchain/build/unittest/SIR/CMakeFiles/SIRTest.dir/SIRFuncTest.cc.o"
  "/home/boyan/simd-toolchain/unittest/SIR/SIRInstrTest.cc" "/home/boyan/simd-toolchain/build/unittest/SIR/CMakeFiles/SIRTest.dir/SIRInstrTest.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/extra/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  "../extra/gtest/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
