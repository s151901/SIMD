# CMake generated Testfile for 
# Source directory: /home/boyan/simd-toolchain
# Build directory: /home/boyan/simd-toolchain/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(lib)
subdirs(tools)
subdirs(scripts)
subdirs(extra/gtest)
subdirs(extra/jsoncpp)
subdirs(test)
subdirs(benchmark)
subdirs(solver)
subdirs(unittest)
subdirs(py-binding)
subdirs(docs)
