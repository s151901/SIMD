# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/Target/BottomUpSchedule.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/BottomUpSchedule.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/DDGSubTree.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/DDGSubTree.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/DataDependencyGraph.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/DataDependencyGraph.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/InterferenceGraph.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/InterferenceGraph.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/PromoteLongImm.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/PromoteLongImm.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/RegAlloc.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/RegAlloc.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetASMParser.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetASMParser.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetBasicInfo.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetBasicInfo.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetBinaryProgram.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetBinaryProgram.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetBlockData.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetBlockData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetCodeGenEngine.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetCodeGenEngine.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetDataLayout.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetDataLayout.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetFuncData.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetFuncData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetFuncRegAllocInfo.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetFuncRegAllocInfo.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetInstrData.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetInstrData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetInstruction.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetInstruction.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetInstructionPacket.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetInstructionPacket.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetIssuePacket.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetIssuePacket.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetModuleData.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetModuleData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetOperand.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetOperand.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetOperationInfo.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetOperationInfo.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/TargetRegisterFile.cc" "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/TargetRegisterFile.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
