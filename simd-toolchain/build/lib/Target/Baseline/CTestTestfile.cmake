# CMake generated Testfile for 
# Source directory: /home/boyan/simd-toolchain/lib/Target/Baseline
# Build directory: /home/boyan/simd-toolchain/build/lib/Target/Baseline
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(ASMParser)
subdirs(CodeGenEngine)
subdirs(SimProcessor)
