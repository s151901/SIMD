# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineBUSelector.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineBUSelector.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineBlockData.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineBlockData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineBypass.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineBypass.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineBypassSelector.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineBypassSelector.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineCallerSavedRegs.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineCallerSavedRegs.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineCodeGenEngine.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineCodeGenEngine.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineCommunication.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineCommunication.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineDataLayout.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineDataLayout.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineFuncData.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineFuncData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineInitCodeEmitter.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineInitCodeEmitter.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineInstrData.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineInstrData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineJointPointAnalysis.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineJointPointAnalysis.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineKernelBuilder.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineKernelBuilder.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineKernelExpressions.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineKernelExpressions.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineModuleData.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineModuleData.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselinePostRATransform.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselinePostRATransform.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineRegAlloc.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineRegAlloc.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineSIRTranslation.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineSIRTranslation.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineSchedule.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineSchedule.cc.o"
  "/home/boyan/simd-toolchain/lib/Target/Baseline/CodeGenEngine/BaselineSpiller.cc" "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CodeGenEngine/CMakeFiles/BaselineCodeGenEngine.dir/BaselineSpiller.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/Target/Baseline/CMakeFiles/BaselineTarget.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Target/CMakeFiles/Target.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  "lib/Target/Baseline/CodeGenEngine/.."
  "../lib/Target/Baseline/CodeGenEngine/.."
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
