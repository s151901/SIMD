# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/DataTypes/ContainerTypes.cc" "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/ContainerTypes.cc.o"
  "/home/boyan/simd-toolchain/lib/DataTypes/Error.cc" "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/Error.cc.o"
  "/home/boyan/simd-toolchain/lib/DataTypes/SIRDataType.cc" "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/SIRDataType.cc.o"
  "/home/boyan/simd-toolchain/lib/DataTypes/SIROpcode.cc" "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/SIROpcode.cc.o"
  "/home/boyan/simd-toolchain/lib/DataTypes/TargetOpcode.cc" "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/TargetOpcode.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
