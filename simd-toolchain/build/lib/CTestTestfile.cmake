# CMake generated Testfile for 
# Source directory: /home/boyan/simd-toolchain/lib
# Build directory: /home/boyan/simd-toolchain/build/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(DataTypes)
subdirs(SIR)
subdirs(Transform)
subdirs(Program)
subdirs(Target)
subdirs(Utils)
subdirs(Simulation)
