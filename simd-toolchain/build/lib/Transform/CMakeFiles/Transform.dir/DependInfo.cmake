# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/Transform/DeadFunctionElimination.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/DeadFunctionElimination.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SIRCallSiteProcessing.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SIRCallSiteProcessing.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SIRFinalize.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SIRFinalize.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SIRFunctionLayout.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SIRFunctionLayout.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SIRLocalOpt.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SIRLocalOpt.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SIRSimplfyBranch.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SIRSimplfyBranch.cc.o"
  "/home/boyan/simd-toolchain/lib/Transform/SplitSIRCallBlock.cc" "/home/boyan/simd-toolchain/build/lib/Transform/CMakeFiles/Transform.dir/SplitSIRCallBlock.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
