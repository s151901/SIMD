# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/SIR/Pass.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/Pass.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRBasicBlock.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRBasicBlock.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRConstant.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRConstant.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRDataObject.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRDataObject.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRExpr.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRExpr.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRFunction.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRFunction.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRInstruction.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRInstruction.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRKernel.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRKernel.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRLoop.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRLoop.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRMemLocation.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRMemLocation.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRModule.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRModule.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRParser.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRParser.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRRegister.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRRegister.cc.o"
  "/home/boyan/simd-toolchain/lib/SIR/SIRValue.cc" "/home/boyan/simd-toolchain/build/lib/SIR/CMakeFiles/SIR.dir/SIRValue.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
