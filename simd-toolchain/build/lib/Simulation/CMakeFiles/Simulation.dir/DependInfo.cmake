# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boyan/simd-toolchain/lib/Simulation/CycleAccurateSimulator.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/CycleAccurateSimulator.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimCore.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimCore.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimMemoryCmd.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimMemoryCmd.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimObjectBase.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimObjectBase.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimProcessor.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimProcessor.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimProgramSection.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimProgramSection.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimSRAM.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimSRAM.cc.o"
  "/home/boyan/simd-toolchain/lib/Simulation/SimSyncChannel.cc" "/home/boyan/simd-toolchain/build/lib/Simulation/CMakeFiles/Simulation.dir/SimSyncChannel.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "__STDC_CONSTANT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boyan/simd-toolchain/build/lib/DataTypes/CMakeFiles/DataTypes.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/lib/Utils/CMakeFiles/Utils.dir/DependInfo.cmake"
  "/home/boyan/simd-toolchain/build/extra/jsoncpp/CMakeFiles/jsoncpp.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../include"
  "../extra/jsoncpp"
  "include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
