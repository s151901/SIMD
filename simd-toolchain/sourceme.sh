#############################################################
#
# START UP SCRIPT TO SET ENVIRONMENT FOR ALL CADENCE TOOLS
# supported on lnx86 platform
# 08/11/2012 EvdH
#
# TO CUSTOMISE THIS SCRIPT FOR YOUR SITE
# 	1) edit line 24 with your license server name
#	2) edit line 27 so that $CDS_TOP points 
#		to your Cadence IC installation directory
#	3)comment or uncomment entire blocks for the tools you use
#
#	4) you may also need to edit the directory names for each tool
#		depending on what you've called them
#
# TO FIND LAUNCH COMMANDS FOR ANY TOOL, USE TO_LAUNCH_CDS_<TOOL>
# E.G. TO_LAUNCH_CDS_EDI
#
# edited 17/09/2012 - setenv K2_VIEWER_HOME no longer required
#
#############################################################

export PS1="\[\033[01;32m\]<cds>\[\033[00m\] \u@\h: \W\$ "
alias ls='ls --color'
alias ll='ls -l'
alias la='ls -a'
alias grep='grep --color'

#to access licences, every user must have the following variable set
export CDS_LIC_FILE=5280@sambava.ics.ele.tue.nl

## script for lnx86 supported tools
export CDS_TOP=/home/eda/Cadence

#############################################################
#
# ALTOS 3.2
# on lnx86 only
#
#############################################################

#ALTOSDA_LICENSE_FILE
export ALTOSHOME=$CDS_TOP/ALTOS_3.2

export PATH=${PATH}:${ALTOSHOME}/bin
export PATH=${PATH}:${ALTOSHOME}/tools/bin
export ALTOS_64=1
export TMPDIR=/tmp
# liberate, lcplot, lwave, variety

#############################################################
#
# ASSURA 4.1 USR2 HF10 in three flavours
# OA 6.15
# or  
# CDB for IC5.1.41
#
#	for startup options, use command
#		assura -help
#
#############################################################

#export CDS_ASSURA=$CDS_TOP/ASSURA_4.12.020_615OA
#export CDS_ASSURA=$CDS_TOP/ASSURA_4.12.020_5.1.41CDB
export CDS_ASSURA=$CDS_TOP/ASSURA41
export ASSURAHOME=$CDS_ASSURA
#the following line might be completely redundant
export SUBSTRATESTORMHOME=$ASSURAHOME		# For Assura-RF
export LANG=C

export PATH=${PATH}:${CDS_ASSURA}/tools/bin
export PATH=${PATH}:${CDS_ASSURA}/tools/assura/bin
export PATH=${PATH}:${SUBSTRATESTORMHOME}/bin

export ASSURA_AUTO_64BIT=ALL

## for CDB version of tools
#alias help_cds_assura  '$CDS_ASSURA/tools/bin/cdsdoc &'
## for OA version of tools 
alias help_cds_assura='$CDS_ASSURA/tools/bin/cdnshelp &'

#############################################################
#
# MVS Manufacturability sign off
# 10.11 HF354
#
#############################################################

export CDS_MVS=$CDS_TOP/MVS_12.10
#for integration with Virtuoso
#enables Launch->DFM option from LayoutXL window
export DFMHOME=$CDS_MVS
export RETHOME=$DFMHOME

export PATH=${PATH}:${CDS_MVS}/tools/bin

alias help_cds_mvs='$CDS_MVS/tools/bin/cdsnhelp &'

alias to_launch_cds_mvs='echo "lpa #Litho Physical Analyzer'


##############################################################
##
## IC -DFWII / VIRTUOSO
## 6.1.5 HF512
## 	launch using 
##		virtuoso
## or
## 5.1.41 HF151
##	launch using 
##		icfb
##
##############################################################

# for OA version
export CDS_IC=$CDS_TOP/IC_6.1.5HF132

# for CDBA version
#export CDS_IC $CDS_TOP/IC/IC_5.1.41HF151 

#this line may be required by the some design kits...
export CDSDIR=$CDS_IC

# When using ADE set netlisting mode to analog
#	("dfIIconfig.pdf"), p16.
export CDS_Netlisting_Mode=Analog
export MG_ENABLE_PTOT=true

# for tutorial material and cadence libraries (eg analogLib)
# the following is also useful
export CDSHOME=$CDS_IC

# for IC 6.1.4 only, comment out for 5.1x
# Set CDS_USE_PALETTE to use Palette Assistant instead of LSW
export CDS_USE_PALETTE


#export PATH "${PATH}:${CDS_IC}/bin"
export PATH=${PATH}:${CDS_IC}/tools/bin
export PATH=${PATH}:${CDS_IC}/tools/dfII/bin

## 6.1x help
alias help_cds_ic='$CDS_IC/tools/bin/cdnshelp &'
## 5.1x help
#alias help_cds_ic  '$CDS_IC/tools/bin/cdsdoc &'


#############################################################
#
# ICC 11.2.41 HF68
# IC 5.1.41 REQUIRES ICC (IC CRAFTSMAN)
# TO BE SET UP FOR SPACE-BASED ROUTING
#
#############################################################


export CDS_ICC=$CDS_TOP/ICC_11.2.41

export PATH=${PATH}:${CDS_ICC}/bin
export PATH=${PATH}:${CDS_ICC}/tools/bin
export PATH=${PATH}:${CDS_ICC}/tools/iccraft/bin
export PATH=${PATH}:${CDS_ICC}/tools/iccraft/lefdef_if/bin
export PATH=${PATH}:${CDS_ICC}/tools/iccraft/gds_if/bin

alias help_cds_icc='$CDS_ICC/tools/bin/cdsdoc &'
alias to_launch_cds_icc='echo "vcar"'

#############################################################
#
# EXT - QRC extraction
# merged into PVE for 2012 release
#
#############################################################

#############################################################
#
# KQV Quick Viewer
# merged into PVE for 2012 release
# 
#
#############################################################

#############################################################
#
# PVE Physical Verification Environment 11.12.106
# includes EXT, QRC and KQV
# launch using
# 	qrc
#	pvs
#	k2_viewer
#
#############################################################

export CDS_PVE=$CDS_TOP/PVE_11.12

export QRC_HOME=$CDS_PVE
#export K2_VIEWER_HOME $CDS_PVE/tools/K2/Viewer_11.1.1HF3

export PATH=${PATH}:${CDS_PVE}/bin
export PATH=${PATH}:${CDS_PVE}/tools/bin

alias help_cds_pve='$CDS_PVE/bin/cdnshelp &'

#############################################################
#
# CCD Conformal Constraints Designer
# 11.10.320
# in 2012, now part of Conformal release stream
#
#############################################################

#############################################################
#
# Conformal
# 11.10.320
#
#############################################################

export CDS_CONFORMAL=$CDS_TOP/CONFRML_11.10.400

export PATH=${PATH}:${CDS_CONFORMAL}/bin

alias help_cds_conformal='$CDS_CONFORMAL/bin/cdnshelp &'
alias to_launch_cds_conformal='echo "lec -LPGXL'
alias to_launch_cds_ccd='echo "ccd'


#############################################################
#
# RC RTL Compiler
# 11.21.000 
#
#############################################################

export RC_PATH=$CDS_TOP/RC_11.21

export PATH=${PATH}:${RC_PATH}/tools/bin

alias help_rc='$RC_PATH/bin/cdnshelp &'
alias to_launch_cds_rc='echo "rc'

#############################################################
#
# EDI Encounter Digital Implementation 
# 11.11
#
#############################################################

export CDS_SOCE=$CDS_TOP/EDI_11.12

export PATH=${PATH}:${CDS_SOCE}/tools/bin

alias help_cds_edi='$CDS_SOCE/tools/bin/cdnshelp &'
alias to_launch_cds_edi='echo "velocity <options> | encounter <options>'


#############################################################
#
# ET Encounter Test
# 10.1HF103
#
#############################################################

export CDS_ET=$CDS_TOP/ET_11.10

export PATH=${PATH}:${CDS_ET}/bin

alias help_cds_et='$CDS_ET/bin/cdnshelp &'
alias to_launch_cds_et='echo "et -help # to see command options"'


#############################################################
#
# ETS Encounter Timing System
# 11.11
# N.B. Encounter Library Characteriser is replaced by Altos Tools
#
#############################################################

export CDS_ETS=$CDS_TOP/ETS_11.12

export PATH=${PATH}:${CDS_ETS}/tools/bin

alias help_cds_ets='$CDS_ETS/bin/cdnshelp &'

alias to_launch_cds_ets='echo "ets #for Encounter Timing System'

#eps #for Encounter Power System\\
#elc #for Encounter Library Characteriser\\
#see page 41 of elcUG.pdf for startup options for library characterizer"'

#############################################################
#
# MMSIM Multimode Simulator
# 11.10.509
#
#############################################################

export CDS_MMSIM=$CDS_TOP/MMSIM_11.10.509

export PATH=${PATH}:${CDS_MMSIM}/tools/bin

alias help_cds_mmsim='$CDS_MMSIM/tools/bin/cdnshelp &'

alias to_launch_cds_mmsim='echo "spectre <options>\\
spectre -help #to display command options\\
ultrasim [-f}<circuit> [options]\\
ultrasim -f #to display command options\\
aps options inputfile\\
aps -help #to display command options"'

##############################################################
##
## INCISIVE 11.10.062 AND 12.1
##
##	for installation help see
##	install_incisive.pdf p12-13
##
##############################################################

export incisiv_dir=$CDS_TOP/INCISIVE_12.10.005

alias help_cds_incisiv='$incisiv_dir/bin/cdnshelp &'

export PATH=${PATH}:${incisiv_dir}/bin
export PATH=${PATH}:${incisiv_dir}/tools/bin
export PATH=${PATH}:${incisiv_dir}/tools/systemc/gcc/bin

#for SOC Verification kit
export SOCV_KIT_HOME=$incisiv_dir/kits/VerificationKit
export PATH=${PATH}:${incisiv_dir}/kits/VerificationKit/bin
source $SOCV_KIT_HOME/env.sh

## tools/bin contains softlinks to all executables

export LANG=C
## previously required for IUS		
#export LD_LIBRARY_PATH $LD_LIBRARY_PATH":$CDS_LDV/tools/lib:$CDS_LDV/tools/systemc/gcc/install/lib"
		
alias to_launch_cds_incisive='echo "irun '

#ncvlog\\
#ncelab\\
#ncsim\\
#nclaunch \\
#ncsc_run # for one-step compilation, elaboration and simulation of SystemC \\
#ncsc  # using the three-step method with SystemC, using ncsc, ncelab and ncsim\\
#n.b. to use ncsc, the environmental variable LD_LIBRARY_PATH may need to be modified \\
# see ncscim.pdf p53 for more info on ncsc \\
#\\
#formalbuild\\
#formalverifier\\
#\\
#emanager -help #to get list of options\\
#eplanner\\
#\\
#specman \\
#specrun \\
#specman GUI can also be launched from irun\\
#\\
##SOC Verification Kit\\
#start_kit #to open the cdnshelp documentation for the kit\\
#start_nav #to open the emanager SOCV kit GUI 'kit_navigator'\\

#start_nav requires emngr to be setup first\\
##Please consult socv_install_config.pdf for setup of workarea"'


###############################################################
###
### VIPCAP Verification IP Catalog
### 11.3.002 Choice of OVM or UVM installs
### must be used with INCISIVE 10.2 or higher
### will not work with INCISIVE 9.2 from EUROPRACTICE 2010 release
###
###############################################################

## for setup and use documentation see cdnshelp:
## <VIPCAT>/tools/bin/cdsnhelp ->VIPCAT 11.30 
##		-> Verification IP -> VIP Catalog User Guide
## and
## <VIPCAT>/tools/bin/cdsnhelp -> VIPCAT 11.30
##		-> Verification IP -> Release Notes
##		-> VIP Catalog Release Alert 
##		-> VIPP 11.3 Release Notes
##		-> Catalog-Wide Issues
##		-> Demos
## or go straight to the pdf:
## <VIPCAT_INSTALL_DIR>/doc/portfolio_user_guide/portfolio_user_guide.pdf

## downloads site ReleaseInfo document p16-17

export CDS_VIPCAT=$CDS_TOP/VIPCAT_11.3.002_OVM
#export CDS_VIPCAT=$CDS_TOP/VIPCAT_11.3.002_UVM

export PATH=${PATH}:${CDS_VIPCAT}/tools/bin
if [ -z "$SPECMAN_PATH" ];
then
    export SPECMAN_PATH=$CDS_VIPCAT/packages
else
    export SPECMAN_PATH=$CDS_VIPCAT/packages:${SPECMAN_PATH}
fi
# if ( $?SPECMAN_PATH == 0) then 
# 	export SPECMAN_PATH=$CDS_VIPCAT/packages
# else 
# 	export SPECMAN_PATH=$CDS_VIPCAT/packages:${SPECMAN_PATH}
# endif
export SPECMAN_PATH=$CDS_VIPCAT/utils:${SPECMAN_PATH}

alias help_cds_vipcat='$CDS_VIPCAT/tools/bin/cdnshelp &'
#alias to_launch_cds_vipcat 'echo "<VIPCAT_inst_dir>/demo.sh -help #to display use modes and list available VIPs \\
#<VIPCAT_inst_dir>/demo.sh <VIP> #to launch demo for a particular VIP"'

########################################
## CTOS 12.10.201
### there is no cdsnhelp for CTOS
### documentation can be found in:
### $CTOS_ROOT/tools/ctos/doc/Cadence_CtoS_User_Guide.pdf p172
##
########################################

export CTOS_ROOT=$CDS_TOP/CTOS_12.10

export PATH=${PATH}:${CTOS_ROOT}/tools/bin

alias help_cds_ctos='acroread $CTOS_ROOT/tools/ctos/doc/Cadence_CtoS_User_Guide.pdf &'
alias to_launch_cds_ctos='echo "ctos or ctosgui"'

##
#########################################
###
### SPB 16.5 HF 005 Systems tools
###
#########################################
##

##for SIP tools
export CDS_SPB=$CDS_TOP/SPB_16.5_HF030
#export CDS_SPB /pack_msc/cadence/dfw2/12/lnx86/SPB_16.5_HF030

##
## first path entry required for RF SIP tools rfwb (RF workbench)
export PATH=${PATH}:${CDS_SPB}/tools/dfII/bin
export PATH=${PATH}:${CDS_SPB}/tools/pcb/bin
export PATH=${PATH}:${CDS_SPB}/tools/specctra/bin
export PATH=${PATH}:${CDS_SPB}/tools/bin
export PATH=${PATH}:${CDS_SPB}/tools/fet/bin

	
alias help_cds_spb='$CDS_SPB/tools/bin/cdnshelp &'
alias to_launch_cds_spb='echo "allegro"'


############################################################
#
# set path
#
############################################################


# order dependencies - rc before EDI
# order dependencies - mvs before IC
# order dependencies - ext before assura, assura after ic

## comment out individual sections to create custom setup
## or use suggested paths below

 

## suggested mixed signal setup (potentially for AMS methodology kit) 
# set path = ($path $mmsimpath $incisivpath $confrmlpath $rcpath $etspath $edipath $icpath $extpath $assurapath)

## suggested digital path
# set path = ($path $incisivpath $ccdpath $confrmlpath $rcpath $etspath $etpath $edipath)

# for systems tools
# set path = ($path $spbpath)

export LM_LICENSE_FILE=$LM_LICENSE_FILE:2100@sambava.ics.ele.tue.nl
source /opt/Xilinx/14.4/ISE_DS/settings64.sh

